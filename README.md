# README #

The following steps are required for setting up the project

### Sofware required ###

* npm
* yarn
* watchman(optional)
* android-studio
* adb

### How do I get set up? ###

* npm install -g react-native-cli 
* cd calin_app
* npm install or yarn install (use only one)
* react-native link (Please note that manual setups may be required for some modules)
* react-native run-android (either on a physical device with USB Debugging enabled or an Android Emulator)
* npm start

### How to build an APK (Debug Build)

In the project root, run the following command

* react-native bundle --dev false --platform android --entry-file index.js --bundle-output ./android/app/build/intermediates/assets/debug/index.android.bundle --assets-dest ./android/app/build/intermediates/res/merged/debug

Then, 

* cd android
* ./gradlew assembleDebug

The APK file would then be available in the directory

* ./android/app/build/outputs/apk/debug/app-debug.apk

### How to build APK (Release Build to be published on playstore)

Follow the steps in the link at 

* https://facebook.github.io/react-native/docs/signed-apk-android

then, publish on playstore using the playstore account(https://play.google.com/apps/publish/)

### Resources/Links ###

* https://facebook.github.io/react-native/docs/getting-started.html
* https://reactjs.org/docs/hello-world.html
* https://reactnavigation.org/docs/en/getting-started.html

### Paystack MetaData  ###

Include the following snippet of code in the RNPaystackModule.java file of the "react-native-paystack" module.

	```java
	ReadableMap metadata = chargeOptions.getMap("metadata");
	try{
		charge.putMetadata("user_id", metadata.getString("user_id"));
		charge.putMetadata("group_id", metadata.getString("group_id"));
		charge.putMetadata("tx_performed_by", metadata.getString("tx_performed_by"));
		charge.putMetadata("keypad_user", String.valueOf(metadata.getBoolean("keypad_user")));
		charge.putMetadata("phone_number", metadata.getString("phone_number"));
		charge.putMetadata("email", metadata.getString("email"));
		charge.putMetadata("full_name", metadata.getString("full_name"));

	} catch (JSONException e){
		e.printStackTrace();
	}
	```

### Who do I talk to? ###

* Moyo
* Tolu
* facebook.github.io
