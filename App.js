import React, { Component } from 'react';

import RootStack from './screens/router';

class App extends Component {

  render(){
    return (
      <RootStack />
    );
  }
}

export default App;