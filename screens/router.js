import React from 'react';
import { 
    createStackNavigator, 
    createDrawerNavigator,
	createSwitchNavigator,
	createAppContainer
} 
from 'react-navigation';

import { 
    YellowBox, 
    Dimensions 
} from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader', 'ListView is deprecated']);

const { width, height } = Dimensions.get('screen');

//Root
import AuthLoadingScreen from './authLoading';

//Auth
import LoginScreen from './auth/login';
import SignUpScreen from './auth/signup';

//Tabs
import SideBar from './components/sidebar';
import HomeScreen from './components/home';
import AccountScreen from './components/account';
import PaymentScreen from './components/payment';


const AuthStack = createStackNavigator({
	Login: LoginScreen,
	SignUp: SignUpScreen,
},{
	headerMode: 'none',
	initialRouteName: 'Login',
});

const DrawerStack = createDrawerNavigator({
    Home: HomeScreen,
	Account: AccountScreen,
	Payment: PaymentScreen
},{
    initialRouteName: 'Home',
	contentComponent: props => <SideBar {...props} />,
	drawerWidth: Math.min(height, width) * 0.7,
	backBehavior: "initialRoute",
})

const RootStack = createSwitchNavigator({
	AuthLoading: AuthLoadingScreen,
	Drawer: DrawerStack,
	Auth: AuthStack,
},
{
  initialRouteName: 'AuthLoading'
});

const AppStack = createAppContainer(RootStack)

export default AppStack