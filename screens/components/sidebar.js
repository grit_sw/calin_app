import React, { Component } from 'react';
import { View, Image, StyleSheet } from "react-native";
import { Text, List, ListItem, Icon, Left } from "native-base";

const routes = [ 
	{name:"Home", icon:"home", nav:"Home"},
	{name:"Account History", icon: "md-mail", nav:"Account"},
	{name:"Buy Electricity", icon: "md-cash", nav:"Payment"}
];

class SideBar extends Component {

    render() {
        return(
            <View style={styles.container}>
				<View style={styles.content}>
					<Image style={styles.logo} source={require('../assets/avatar.png')} resizeMode='contain' />
					<Text style={styles.profile}>Olamide Trent</Text>
				</View>
				<List
					dataArray={routes}
					renderRow={data => {
						return (
							<ListItem
								button
								onPress={() => this.props.navigation.navigate(data.nav)}>
								<Left>
									<Icon 
										active
										name={data.icon}
										style={styles.icon}
										/>
									<Text style={styles.text}>{data.name}</Text>
								</Left>
							</ListItem>
							
						);
					}}
				/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	content: {
		marginBottom: 30,
		marginTop: 20,
		alignItems: 'center',
	},
	logo: {
		height: 120,
		width: 120,
	},
	profile: {
		padding: 10,
	},
    icon: {
        color: "#00b490", 
        fontSize: 26, 
        width: 30
    },
    text: {
        fontWeight: "400",
        fontSize: 16,
        marginLeft: 20
    }
});

export default SideBar;
