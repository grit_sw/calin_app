import React, { Component } from 'react';
import {
    findNodeHandle,
    UIManager, 
    Alert, 
    StatusBar,
} from 'react-native';
import {
    Header,
    Left,
    Button,
    Body,
    Title,
    Right,
    Icon,
 } from 'native-base';
 import AsyncStorage from '@react-native-community/async-storage';

class HeaderComponent extends Component{

    constructor(props){
        super(props)
        this.state = {
            icon: null,
        }
    }

   
    _signOut = async () => {
		await AsyncStorage.removeItem('userJSON'); 
        this.props.navigation.navigate('Auth');
    }

    _onPress = () => {
            if (this.state.icon) {
                UIManager.showPopupMenu(
                    findNodeHandle(this.state.icon),
                    ["Sign Out"],
                    this.onError,
                    this.onMorePress
                )
            }
        }

    onMorePress = (eventName, index) => {
        if (eventName !== 'itemSelected') return;
        else if (index == 0) {
            Alert.alert(
                "Sign Out",
                "Do you really want to sign out?",
                [
                    {text: "Cancel", onPress: () => {}, style: 'cancel'},
                    {text: "Sure", onPress: this._signOut},
                ]
            )
        }
    }

    onError = () => {
        console.log('Popup Error')
    }

    onRef = icon => {
        if (!this.state.icon){
            this.setState({icon})
        }
    }

    render(){
        return(
            <Header
                style={{ backgroundColor: "#00b490" }}>
                <StatusBar animated={true} backgroundColor={"#00b490"}/>
                <Left>
                    <Button
                        transparent
                        style={{height: 40}}
                        active={true}
                        onPress={() => this.props.navigation.openDrawer()}
                        >
                        <Icon  style={{ color: 'white', }} name="menu" />
                    </Button>
                </Left>
                <Body>
                    <Title style={{width: 200}}>
                        {this.props.headerName}
                    </Title>
                </Body>
                <Right>
                    <Button 
                        transparent
                        style={{height: 40}}
                        onPress={this._onPress}>
                        <Icon  ref={this.onRef} name="more"/>
                    </Button>
                </Right>
            </Header>
        )
    }
}

export default HeaderComponent;