import React, { Component } from 'react';
import {
    Alert,
    AsyncStorage,
    StyleSheet,
	StatusBar,
	View,
	Text,
	Button
 } from 'react-native';
 import HeaderComponent from './header';

class HomeScreen extends Component {


    render(){
        return (
            <View>
				<HeaderComponent navigation={this.props.navigation} headerName={"Home"} />
				<Text>Home (Data Reading)</Text>
			</View>
        )
    }
}

const styles = StyleSheet.create({
    modalBackground: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
      backgroundColor: '#FFFFFF',
      height: 200,
      width: 300,
      borderRadius: 10,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around'
    }
});

export default HomeScreen;