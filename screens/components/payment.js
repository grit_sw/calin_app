import React, { Component } from 'react';
import {
	StyleSheet,
	View,
	Text,
	TextInput,
	TouchableOpacity,
	ScrollView,
	Alert,
} from 'react-native';
import HeaderComponent from './header';
import RNPaystack from 'react-native-paystack';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from './loader';

RNPaystack.init({ "publicKey": "pk_test_585b053fa4f3fb743546f05cf235aeaec455d4c1" })

class PaymentScreen extends Component {

	constructor() {
		super();
		this.state = {
			cardNumber: "4084084084084081",
            CVV: "408",
            M: "12",
            Y: "19",
			amount: "1000",
			loading: false
		}
	}

	onPress = async () => {
		let det = {
            cardNumber: this.state.cardNumber,
            CVV: this.state.CVV,
            M: this.state.M,
            Y: this.state.Y,
            amount: this.state.amount,
		}
		
        if (det.cardNumber.length < 16 || det.CVV.length < 3 || det.M.length < 2 || det.Y.length < 2 || det.amount.length < 3 || parseInt(det.amount, 10) <= 0 ){
			alert("Please Check the entry")
			return
		}
		
		//start the payment process
		this.setState({loading: true})
		let email="apps@grit.systems"

		await RNPaystack.chargeCard({
			cardNumber: this.state.cardNumber,
			expiryMonth: this.state.M,
			expiryYear: this.state.Y,
			cvc: this.state.CVV,
			email: email,
			amountInKobo: parseInt(this.state.amount, 10) * 100,
			metadata: {
				user_id: "user_id",
				group_id: "group_id",
				tx_performed_by: "user_id",
				keypad_user: false,
				phone_number: "phone_number",
				email: email,
				full_name: "full_name",
			}
			// metadata: {
			// 	user_id: this.state.user_details.user_id,
			// 	group_id: this.state.user_details.group_id,
			// 	tx_performed_by: this.state.user_details.user_id,
			// 	keypad_user: this.state.user_details.keypad_user,
			// 	phone_number: this.state.user_details.phone_number,
			// 	email: email,
			// 	full_name: this.state.user_details.full_name,
			// }
		})
		.then( response => {
			fetch(`https://api.paystack.co/transaction/verify/${response.reference}`,{
				method: "GET",
				headers: {
					"Authorization": "Bearer sk_test_1a3f75fd24da473766d6a0efdf05ddaaf1a59c9c",
					"Content-Type": "application/json",
				}
			})
			.then(res => res.json())
			.then(async resJson => {
				this.setState({loading: false})
				if (resJson.status){//payment successful
					await AsyncStorage.setItem('paystack_auth', JSON.stringify(resJson.data.authorization))
					// Alert.alert("Status", `${JSON.stringify(resJson.data)}`);
					Alert.alert("Status", `${resJson.data.gateway_response}`);
				}
				else{
					Alert.alert("Status", JSON.stringify(resJson));
				}
			})
			.catch(error => {
				this.setState({loading:false});
				Alert.alert("Status", error.message);
			})
		})
		.catch(error => {
			this.setState({loading: false})
			Alert.alert("Status", error.message)
		})
	}
	

	render() {
		return (
			<View>
				<HeaderComponent navigation={this.props.navigation} headerName={"Buy Electricity"} />
				<Loader loading={this.state.loading} />
				<Text style={styles.description}>Enter your card details below</Text>
				<ScrollView contentContainerStyle={{ alignItems: 'center' }} showsVerticalScrollIndicator={false} style={{ marginTop: 0, margin: 30 }}>
					<View style={styles.section}>
						<Text style={styles.label}>Card Number</Text>
						<TextInput value={this.state.cardNumber} onChangeText={cardNumber => this.setState({cardNumber})} keyboardType={'numeric'} placeholder={'Card Number'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>CVV</Text>
						<TextInput value={this.state.CVV} onChangeText={CVV => this.setState({CVV})} keyboardType={'numeric'} placeholder={'CVV'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Expiry Date</Text>
						<TextInput value={this.state.M} onChangeText={M => this.setState({M})} keyboardType={'numeric'} placeholder={'MM'} style={{ ...styles.input, width: "30%", }} />
						<TextInput value={this.state.Y} onChangeText={Y => this.setState({Y})} keyboardType={'numeric'} placeholder={'YY'} style={{ ...styles.input, width: "30%", }} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Amount in ₦</Text>
						<TextInput value={this.state.amount} onChangeText={amount => this.setState({amount})} keyboardType={'numeric'} placeholder={'Amount'} style={styles.input} />
					</View>
					<TouchableOpacity style={styles.button} onPress={() => {
						Alert.alert(
							"Transaction Alert", 
							`You will be charged #${this.state.amount} for this transaction`,
							[
								{text: 'Cancel', onPress: () => {}, style: 'cancel'},
								{text: "Agree", onPress: this.onPress},
							],
							{ cancelable: false }
						)
					}}>
						<Text style={styles.buttonText}>
							Pay
					</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{...styles.button, backgroundColor: 'red'}} onPress={() => {
						this.setState({
							cardNumber: "",
							CVV: "",
							M: "",
							Y: "",
							amount: ""
						});
					}}>
						<Text style={styles.buttonText}>
							Clear Entry
						</Text>
					</TouchableOpacity>
					{/* <TouchableOpacity style={styles.misc} onPress={() => {this.props.navigation.navigate('Cards')}}>
					<Text>
						{"Already have a saved card? "}
						<Text style={{ textDecorationLine: 'underline'}}>Pay here</Text>
					</Text>
				</TouchableOpacity> */}
				</ScrollView>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		margin: 30
	},
	content: {
		marginBottom: 20,
	},
	logo: {
		height: 100,
		width: 100,
	},
	logoname: {
		textAlign: 'center',
		width: 120,
		height: 40,
		fontWeight: 'bold',
		color: '#011c25',
		fontSize: 18,
	},
	description: {
		marginTop: 30,
		height: 40,
		width: '100%',
		textAlign: 'center',
	},
	input: {
		marginTop: 20,
		marginRight: 20,
		width: '100%',
		borderBottomWidth: 1,
		color: 'grey'
	},
	label: {
		marginTop: 20,
		borderBottomWidth: 0,
		width: 100,
		fontWeight: 'bold',
		alignSelf: 'center',
		marginRight: 0
	},
	section: {
		flexDirection: 'row',
		alignSelf: 'flex-start'
	},
	misc: {
		marginTop: 50,
	},
	button: {
		marginTop: 40,
		height: 40,
		width: '60%',
		backgroundColor: '#00b07e',
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#fff'
	}
});

export default PaymentScreen;