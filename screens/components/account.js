import React, { Component } from 'react';
import {
    StyleSheet,
	View,
	ScrollView
 } from 'react-native';
 import {
	List,
	ListItem,
	Text,
	Left,
	Right,
	Body
} from "native-base";
 import HeaderComponent from './header';

 const datas = [
	{
		amount: 3000,
		units: 10000,
		time: "3:43 pm",
		token: 1234567890
	},
	{
		amount: 200,
		units: 500,
		time: "1:12 pm",
		token: 2987654321
	},
	{
		amount: 500,
		units: 1000,
		time: "10:03 am",
		token: 4793843934  
	},
	{
		amount: 3000,
		units: 10000,
		time: "11:11 pm",
		token: 7881871790
	},
	{
		amount: 2000,
		units: 5000,
		time: "11:11 pm",
		token: 4337656567
	},
	{
		amount: 1000,
		units: 2500,
		time: "8:54 pm",
		token: 2478727479
	}
];
class AccountScreen extends Component {


    render(){
        return (
            <View>
				<HeaderComponent navigation={this.props.navigation} headerName={"Account History"}/>
				<ScrollView>
					<List
						dataArray={datas}
						renderRow={data =>
							<ListItem avatar>
								<Left style={{flexDirection: 'column' }}>
									<Text note>
										{(new Date()).toLocaleDateString()}
									</Text>
									<Text note>
										{(new Date()).toLocaleTimeString()}
									</Text>
								</Left>
								<Body>
									<Text style={{fontWeight: 'bold'}}>
										{data.token}
									</Text>
									<Text numberOfLines={1} note>
										<Text note>{"Units (₦/KWh): "}</Text>{data.units}
									</Text>
								</Body>
								<Right>
									<Text>
										₦{data.amount}
									</Text>
								</Right>
							</ListItem>
						}/>
					</ScrollView>
			</View>
        )
    }
}

const styles = StyleSheet.create({
    

});

export default AccountScreen;