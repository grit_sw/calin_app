import React, { Component } from 'react';
import {
	StyleSheet,
	Image,
	View,
	ScrollView,
	Text,
	TextInput,
	TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from '../components/loader';

const base_url = 'http://dev-backend.gtg.grit.systems:5555';

class LoginScreen extends Component {

	constructor() {
		super()
		this.state = {
			"loading": false,
			"user_id": "workshop@grit.systems",
			"password": "haha",
		}
	}
 
	onPress = async () => {

		this.setState({loading: true})
		fetch(base_url+'/auth/login/', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json'
			},
			body: JSON.stringify({
				"user_id": this.state.user_id,
				"password": this.state.password
			}),
		
		})
		.then(res => res.json())
		.then( async resJson => {

			if (resJson["success"]) {
				await AsyncStorage.setItem('userJSON', JSON.stringify(resJson))
				let x = (new Date()).getTime()
				await AsyncStorage.setItem('lastLoginTime', x.toString())
				
				this.setState({loading: false})
				//  alert("success")
				this.props.navigation.navigate('Home')
			} else {
				this.setState({loading: false})
				// alert("Please check your credentials")
				alert(JSON.stringify(resJson))
			}
		}) 
		.catch(error => {
			this.setState({loading: false})
			alert(error.message)
		});
		
	}
 
	 render(){
		 return (
			 <View style={styles.container}>
				 <Loader loading={this.state.loading} />
				<View style={styles.content}>
					<Image style={styles.logo} source={require('../assets/logo.png')} resizeMode='contain' />
				</View>
				<Text style={styles.logoname}>GRIT Systems</Text>
				<Text style={styles.description}>Enter your details to get access</Text>
				<ScrollView contentContainerStyle={{ alignItems: 'center' }} showsVerticalScrollIndicator={false} style={{width:"100%" }}>
					<View style={styles.section}>
						<Text style={styles.label}>Email</Text>
						<TextInput value={this.state.user_id} onChangeText={user_id => this.setState({user_id})} keyboardType={'email-address'} placeholder={'Email'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Password</Text>
						<TextInput secureTextEntry value={this.state.password} onChangeText={password => this.setState({password})} keyboardType={'ascii-capable'} placeholder={'Password'} style={styles.input} />
					</View>
					{/* <TouchableOpacity style={styles.misc} onPress={() => {}}>
						<Text>
							Forgot Password?
						</Text>
					</TouchableOpacity> */}
					<TouchableOpacity style={styles.button} onPress={this.onPress}>
						<Text style={styles.buttonText}>
							Login
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.misc} onPress={() => {this.props.navigation.navigate('SignUp')}}>
						<Text>
							{"Don't have an Account? "}
							<Text style={{ textDecorationLine: 'underline'}}>Sign Up</Text>
						</Text>
					</TouchableOpacity>
				</ScrollView>
			</View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
		flex: 1,
		alignItems: 'center',
		margin: 30
	},
	content: {
		marginBottom: 20,
	},
	logo: {
		height: 100,
		width: 100,
	},
	logoname: {
		textAlign: 'center',
		width: 120,
		height: 40,
		fontWeight: 'bold',
		color: '#011c25',
		fontSize: 18,
	},
	description: {
		marginTop: 30,
		height: 40,
		width: '100%',
		textAlign: 'center',
	},
	input: {
		marginTop: 20,
		marginRight: 20,
		width: '100%',
		borderBottomWidth: 1,
		color: 'grey'
	},
	label: {
		marginTop: 20,
		borderBottomWidth: 0,
		width: 100,
		fontWeight: 'bold',
		alignSelf: 'center',
		marginRight: 0,
		// color: '#00b492'
	},
	section: {
		flexDirection: 'row',
		alignSelf: 'flex-start'
	},
	misc: {
		marginTop: 50,
	},
	button: {
		marginTop: 30,
		height: 40,
		width: '60%',
		backgroundColor: '#00b07e',
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#fff'
	}
});

export default LoginScreen;