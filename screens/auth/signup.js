import React, { Component } from 'react';
import {
    StyleSheet,
	Image,
	View,
	Text,
	TextInput,
	TouchableOpacity,
	ScrollView,
	Picker,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Loader from '../components/loader';

const base_url = 'http://dev-backend.gtg.grit.systems:5555';

class SignUpScreen extends Component {

	constructor() {
		super()
		this.state = {
			"loading": false,
			"first_name": "first",
			"last_name": "test",
			"title": "Mr",
			"email": "first@email.com",
			"phone_number": "07081234567",
			"meter_number": "12345",
			"password": "string",
			"confirm_password": "string"
		}
	}

	onPress = async () => {

		this.setState({loading: true})
        fetch(base_url+'/auth/user-signup/', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify({
				...this.state
            }),
           
        })
        .then(res => res.json())
        .then( async resJson => {
			
			if (resJson["success"]) {
				await AsyncStorage.setItem('userJSON', JSON.stringify(resJson))
				let x = (new Date()).getTime()
				await AsyncStorage.setItem('lastLoginTime', x.toString())
				
				this.setState({loading: false})
				//  alert("success")
				this.props.navigation.navigate('Home')
			} else {
				this.setState({loading: false})
				// alert("Please check your credentials")
				alert(JSON.stringify(resJson))
			}
        }) 
        .catch(error => {
			this.setState({loading: false})
			alert(error.message)
        });
	}

    render(){
        return (
            <View style={styles.container}>
				<Loader loading={this.state.loading} />
				<ScrollView contentContainerStyle={{ alignItems: 'center' }} showsVerticalScrollIndicator={false} style={{width:"100%"}}>
					<View style={styles.content}>
						<Image style={styles.logo} source={require('../assets/logo.png')} resizeMode='contain' />
					</View>
					<Text style={styles.logoname}>GRIT Systems</Text>
					<Text style={styles.description}>Start out by creating a new account</Text>
				

					<View style={styles.section}>
						<Text style={styles.label}>Title</Text>
						<Picker
							selectedValue={this.state.title}
							style={{...styles.input, height: 50, width: 100}}
							onValueChange={(itemValue, itemIndex) =>
								this.setState({title: itemValue})
							}>
							<Picker.Item label="Mr" value="Mr" />
							<Picker.Item label="Ms" value="Ms" />
						</Picker>			
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>First Name</Text>
						<TextInput value={this.state.first_name} onChangeText={first_name => this.setState({first_name})} placeholder={'First Name'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Last Name</Text>
						<TextInput value={this.state.last_name} onChangeText={last_name => this.setState({last_name})} placeholder={'Last Name'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Email</Text>
						<TextInput value={this.state.email} onChangeText={email => this.setState({email})} keyboardType={'email-address'} placeholder={'Email'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Phone No</Text>
						<TextInput value={this.state.phone_number} onChangeText={phone_number => this.setState({phone_number})} keyboardType={'numeric'} placeholder={'Phone No'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Meter No</Text>
						<TextInput value={this.state.meter_number} onChangeText={meter_number => this.setState({meter_number})} keyboardType={'numeric'} placeholder={'Meter No'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Password</Text>
						<TextInput secureTextEntry value={this.state.password} onChangeText={password => this.setState({password})} keyboardType={'ascii-capable'} placeholder={'Password'} style={styles.input} />
					</View>
					<View style={styles.section}>
						<Text style={styles.label}>Confirm Password</Text>
						<TextInput secureTextEntry value={this.state.confirm_password} onChangeText={confirm_password => this.setState({confirm_password})} keyboardType={'ascii-capable'} placeholder={'Confirm Password'} style={styles.input} />
					</View>

					<TouchableOpacity style={styles.button} onPress={this.onPress}>
						<Text style={styles.buttonText}>
							Sign Up
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={{...styles.button, backgroundColor: 'red'}} onPress={() => {
						this.setState({
							"first_name": "",
							"last_name": "",
							"title": "",
							"email": "",
							"phone_number": "",
							"meter_number": "",
							"password": "",
							"confirm_password": ""
						});
					}}>
						<Text style={styles.buttonText}>
							Clear Entry
						</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.misc} onPress={() => {this.props.navigation.navigate('Login')}}>
						<Text>
							{"Already have an Account? "}
							<Text style={{ textDecorationLine: 'underline'}}>Login</Text>
						</Text>
					</TouchableOpacity>
				</ScrollView>
			</View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
		flex: 1,
		alignItems: 'center',
		margin: 30
	},
	content: {
		marginBottom: 20,
	},
	logo: {
		height: 100,
		width: 100,
	},
	logoname: {
		textAlign: 'center',
		width: 120,
		height: 40,
		fontWeight: 'bold',
		color: '#011c25',
		fontSize: 18,
	},
	description: {
		marginTop: 30,
		height: 40,
		width: '100%',
		textAlign: 'center',
	},
	input: {
		marginTop: 20,
		marginRight: 20,
		width: '100%',
		borderBottomWidth: 1,
		color: 'grey'
	},
	label: {
		marginTop: 20,
		borderBottomWidth: 0,
		width: 100,
		fontWeight: 'bold',
		alignSelf: 'center',
		marginRight: 0,
		// color: '#00b492'
	},
	section: {
		flexDirection: 'row',
		alignSelf: 'flex-start'
	},
	misc: {
		marginTop: 50,
	},
	button: {
		marginTop: 30,
		height: 40,
		width: '60%',
		backgroundColor: '#00b07e',
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonText: {
		fontSize: 16,
		fontWeight: 'bold',
		color: '#fff'
	}
});

export default SignUpScreen;