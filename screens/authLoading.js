import React, { Component } from 'react';
import { 
	Image,
	Text,
	View,
	StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

class AuthLoadingScreen extends Component {
    constructor(props){
        super(props);
    }

    async componentDidMount() {

		try {
			const value = await AsyncStorage.getItem('userJSON')
			if (value !== null) { //user has logged in before
				// alert(value)
				let ms23 = 1000 * 60 * 60 * 23
				let cur = (new Date()).getTime()
				let llt = await AsyncStorage.getItem('lastLoginTime', (err, val) => {return val}) || "0"
				let diff = cur - Number(llt)
				if (diff >= ms23){
					//user has not logged on in the last 24 hrs
					this.props.navigation.navigate('Auth');
					return;
				} 
				this.props.navigation.navigate('Home');
			} else {
				this.props.navigation.navigate('Auth');
			}
		} catch (error) {
			alert(error.message)
		}
	}
	

    render() {
        return (
            <View style={styles.container}>
				<View style={styles.content}>
					<Image style={styles.logo} source={require('./assets/logo.png')} resizeMode='contain' />
				</View>
				<Text style={styles.logoname}>GRIT Systems</Text>
			</View>
        );
    }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	content: {
		marginBottom: 20,
	},
	logo: {
		height: 100,
		width: 100,
	},
	logoname: {
		textAlign: 'center',
		width: 120,
		height: 40,
		fontWeight: 'bold',
		color: '#011c25',
		fontSize: 18,
	},
})
  
export default AuthLoadingScreen;
